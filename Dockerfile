# Start with .Net Core SDK Ubuntu
FROM mcr.microsoft.com/dotnet/core/sdk:3.1-focal

# Install Python
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
		python3 \
		python3-setuptools \
		python3-pip \
        python3-dev

# Default into a bash shell 
# since this will be used for development purposes
CMD ["bash"]
